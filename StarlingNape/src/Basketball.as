package
{
	import flash.display.Loader;
	
	import nape.phys.Body;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Basketball extends Sprite
	{
		[Embed(source="basketball.png")]
		private var basketball:Class;

		public function Basketball(text)
		{
			this.width = 50;
			this.height = 50;
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			addChild(Image.fromBitmap(new basketball()));
		}
	}
}