package
{
	import nape.phys.Body;
	
	import starling.display.Image;
	import starling.display.Sprite;
	public class Water extends Sprite
	{
		[Embed(source="water.png")]
		private var water:Class;
		public function Water()
		{
			//this.pivotX = this.width >> 1;
			//this.pivotY = this.height >> 1;
			this.y=400;
			addChild(Image.fromBitmap(new water()));
		}
	}
}