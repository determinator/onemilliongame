package
{
	import nape.phys.Body;
	
	import starling.display.Image;
	import starling.display.Sprite;
	public class Treasure extends Sprite
	{
		[Embed(source="treasures.png")]
		private var treasure:Class;
		public function Treasure()
		{
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			addChild(Image.fromBitmap(new treasure()));
		}
	}
}