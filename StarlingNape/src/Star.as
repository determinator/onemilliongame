package
{
	import nape.phys.Body;
	
	import starling.display.Image;
	import starling.display.Sprite;
	public class Star extends Sprite
	{
		[Embed(source="star.png")]
		private var star:Class;
		public function Star()
		{
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			addChild(Image.fromBitmap(new star()));
		}
	}
}