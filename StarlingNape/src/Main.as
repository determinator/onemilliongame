package
{	
	import flash.display.Shape;
	import flash.display.Stage;
	import flash.events.AccelerometerEvent;
	import flash.events.MouseEvent;
	import flash.sensors.Accelerometer;
	import flash.text.TextField;
	
	import nape.callbacks.CbType;
	import nape.constraint.PivotJoint;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.geom.Vec2List;
	import nape.phys.Body;
	import nape.phys.BodyList;
	import nape.phys.BodyType;
	import nape.phys.FluidProperties;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.shape.Shape;
	import nape.space.Space;
	import nape.util.ShapeDebug;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Main extends Sprite
	{	
		[Embed(source="back.jpg")]
		private var back:Class;
		
		private var space:Space;
		private var water:Body;
		private var waterShape:nape.shape.Shape;
		private var box:Body;
		private var ship:Image;
		private var ball:Body;
		private var duck:Body;
		private var hand:PivotJoint;
		private var tx:Number;
		private var ty:Number;
		private var touch:Touch;
		private var ns:Stage;
		private var fy:Number;
		private var t:Number;
		private var debug:ShapeDebug;
		private var stars;
		private var treasures;
		
		
		
		public function Main()
		{	
			addEventListener(Event.ADDED_TO_STAGE, init);
			t = 0;
			fy = 0;
		}
		
		private function init(event:Event):void
		{
			addChild(Image.fromBitmap(new back()));
			stars = [];
			for(var i=0;i<10; i++){
				stars[i] = new Star();
				stars[i].x = i*100;
				stars[i].y = 100;
				addChild(stars[i]);
			}
			
			treasures = [];
			for(var i=0;i<3; i++){
				treasures[i] = new Treasure();
				treasures[i].x =  1000 + i*500;
				treasures[i].y = 500;
				addChild(treasures[i]);
			}
			
			ns = Starling.current.nativeStage;
			
			space = new Space(new Vec2(0, 3000));
			
			var floor:Body = new Body(BodyType.STATIC); 
			floor.shapes.add(new Polygon(Polygon.rect(0, 600, 800, 20)));
			floor.space = space;
			
			hand = new PivotJoint(space.world,null,new Vec2(),new Vec2());
			hand.active = false;
			hand.stiff = false;
			hand.space = space;
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			ns.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			ns.addEventListener(MouseEvent.MOUSE_UP, onUp);
			
			var acc:Accelerometer = new Accelerometer();
			acc.addEventListener(AccelerometerEvent.UPDATE, onUpdate);
			ball = addBall();
			
			
			water = new Body(BodyType.STATIC);//, new Vec2(0, 300));
			
			var vList:Vec2List = new Vec2List();
			var width:int = 800;
			var height:int  = 600;
			for(var i:int=0;i<100;i++)
			{
				var x = 8;
				var y = Math.sin(x*i*0.01)*20;
				trace(x*i +' - '+y);
				vList.add(new Vec2(x*i,300+y));
			}
			
			vList.add(new Vec2(800,400));
			vList.add(new Vec2(0,400));
			waterShape = new Polygon(vList, new Material(0,1.0,1,1000,0.01));
			waterShape.fluidEnabled = true;
			waterShape.fluidProperties = new FluidProperties(3000,1);
			water.shapes.add(waterShape);
			
			waterShape = new Polygon(Polygon.rect(0, 400, 800, 300), new Material(0,1.0,1,1000,0.01));
			waterShape.fluidEnabled = true;
			waterShape.fluidProperties = new FluidProperties(3000,1)
			water.shapes.add(waterShape);
			water.space = space;
			
			water.graphic = new Water();		
			water.graphicUpdate = updateGraphics;
			
			
			addChild(water.graphic);
			
			
			debug = new ShapeDebug(800, 480, 0x33333333);
			debug.draw(space);
			
			var MovieClipDebug:flash.display.MovieClip = new flash.display.MovieClip();
			MovieClipDebug.addChild(debug.display);
			Starling.current.nativeOverlay.addChild(MovieClipDebug);
			
			var myText:TextField = new TextField();
			myText.text = "Republic of Code";
			myText.x = 100;
			myText.y = 100;
			//addChild(myText);
			
		}
		
		protected function onUpdate(event:AccelerometerEvent):void
		{
			space.gravity = new Vec2(-event.accelerationX*5000, 3000);
		}
		
		protected function onUp(event:MouseEvent):void
		{
			hand.active = false;
			fy = 0;
		}
		
		protected function onDown(event:MouseEvent):void
		{
			fy = 30;
		}
		
		private function onEnterFrame(event:Event):void
		{
			/*water.shapes.remove(waterShape);
			var vList:Vec2List = new Vec2List();
			var width:int = 800;
			var height:int  = 600;
			for(var i:int=0;i<100;i++)
			{
				var x = 8;
				var y = Math.sin(x*i*0.01+t)*20;
				trace(x*i +' - '+y);
				vList.add(new Vec2(x*i,300+y));
			}
			
			vList.add(new Vec2(800,400));
			vList.add(new Vec2(0,400));
			waterShape = new Polygon(vList, new Material(0,1.0,1,1000,0.01));
			waterShape.fluidEnabled = true;
			waterShape.fluidProperties = new FluidProperties(3000,1);
			water.shapes.add(waterShape);*/
			
			
			/*waterShape = new Polygon(Polygon.rect(0, 400, 800, 300), new Material(0,1.0,1,1000,0.01));
			waterShape.fluidEnabled = true;
			waterShape.fluidProperties = new FluidProperties(3000,1)
			water.shapes.add(waterShape);
			water.space = space;*/
			
		//	water.graphic = new Water();		
		//	water.graphicUpdate = updateGraphics;
			
			
		//	addChild(water.graphic);
			
			
			hand.anchor1.setxy(ns.mouseX, ns.mouseY);
			space.step(1/60);
			t++;
			debug.clear();
			debug.draw(space);
			debug.flush();
			
			for(var i=0;i<stars.length;i++){
				if(stars[i].x<-20 || dist(ball.position.x, ball.position.y+25, stars[i].x, stars[i].y)<50){			
					stars[i].x = 1000;
					stars[i].y = Math.floor(600*Math.random());
				}else{
					stars[i].x-=10;
				}
			}
			
			for(var i=0;i<treasures.length;i++){
				if(treasures[i].x<-20 || dist(ball.position.x, ball.position.y+25, treasures[i].x, treasures[i].y)<50){			
					treasures[i].x = 1000;
					treasures[i].y = Math.floor(380 + 100*Math.random());
				}else{
					treasures[i].x-=10;
				}
			}
			
			if(ball.position.y>280 && fy)
				ball.applyLocalForce(space.gravity.mul(ball.gravMass*3),ball.localCOM)
			t++;
		}
		private function dist(x1,y1,x2,y2){
			return Math.sqrt(Math.pow(x1-x2,2) + Math.pow(y1-y2,2));
		}
		private function addBall():Body
		{
		
			ball = new Body(BodyType.DYNAMIC, new Vec2(100, 400));
			
			ball.shapes.add((new Circle(51, null, new Material(1,1.0,1.4,800,0.01))));
			ball.space = space;		
			ball.graphic = new Basketball("Ball");		
			ball.graphicUpdate = updateGraphics;
			addChild(ball.graphic);
			
			return ball;
		}
		
		private function updateGraphics(b:Body):void
		{
			b.graphic.x = b.position.x;
			b.graphic.y = b.position.y;
			b.graphic.rotation = b.rotation;
		}
	}
}